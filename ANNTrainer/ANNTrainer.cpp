#include <iostream>
#include <ANN.h>
#include <time.h>
using namespace std;
using namespace ANN;

int main()
{
	/*cout << "hello ANN!" << endl;
	cout << GetTestString().c_str() << endl;*/
	/*
	1)������ ��������� ������ �� �����(ANN::LoadData)
	2)������� ��������� ���� ������������ ������������(ANN::CreateNeuralNetwork)
	3)������� ��������� ����(ANN::BackPropTraining)
	4)������� ���������� � ���� ��������� ����(ANN::ANeuralNetwork::GetType)
	5)��������� ��������� ��������� ���� � ��������� ����(ANN::ANeuralNetwork::Save)*/


	vector<vector<float>> input;
	vector<vector<float>> out;
	//1)������ ��������� ������ �� �����(ANN::LoadData)C:\Users\user\Desktop\machine_learning\machine_learning_fzf\Debug
	//LoadData( "./Debug/xor.data", input, out );
	if (LoadData("C:/Users/user/Desktop/machine_learning/machine_learning_fzf/Debug/xor.data", input, out))
	{
		cout << "Loaded" << endl;
	}
	vector<size_t> Configuration = vector<size_t>();//������� ������������ ����
	Configuration.push_back(2);//1-�� ������� ����  ���� � ����� ���������
	Configuration.push_back(10);
	Configuration.push_back(10);
	Configuration.push_back(1);//1 �������� ����
	//2)������� ��������� ���� ������������ ������������(ANN::CreateNeuralNetwork)
	ANeuralNetwork::ActivationType activation_type = ANeuralNetwork::POSITIVE_SYGMOID;

	shared_ptr<ANeuralNetwork> Ann = CreateNeuralNetwork(Configuration, activation_type, 1);
	//CreateNeuralNetwork(Configuration,Activation_type,1);

	//3)������� ��������� ����(ANN::BackPropTraining)
	BackPropTraining(Ann,input, out, 10000, 0.001,0.1,true);// ������� ���� ������� ��������� ��������������� ������

	//4)������� ���������� � ���� ��������� ����(ANN::ANeuralNetwork::GetType)
	cout << Ann->GetType() << endl;

	//5)��������� ��������� ��������� ���� � ��������� ����(ANN::ANeuralNetwork::Save)
	Ann->Save("./Debug/Ann.txt");
	return 0;
	
}