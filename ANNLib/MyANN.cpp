#include "MyANN.h"

#include <fstream>
#include <iomanip>
#include <iostream>
#include <fstream>
#include <stdlib.h>
#define ANNDLL_EXPORTS


std::shared_ptr<ANN::ANeuralNetwork> ANN::CreateNeuralNetwork(std::vector<size_t>& configuration, ANeuralNetwork::ActivationType activation_type, float scale)
{
	return std::make_shared<MyANN>(configuration, scale, activation_type);
}


 std::string MyANN::GetType()
{
	return std::string("Zhirayr Dabagyan");
}

 std::vector<float> MyANN::Predict(std::vector<float>& input)
{ 
	//return std::vector<float>();
	 if (!is_trained || configuration.empty() || configuration[0] != input.size())
	 {
		 cout << "Problems!" << endl;
	 }

	 

	 vector<float> prev_out = input;//����, ����� ������ ��������������� ���������� �������� �� ������� ����.
	 vector<float> cur_out; //����� ����, ����� ������������� ���������� �������� � �������� ����

	 for (size_t layer_idx = 0; layer_idx < configuration.size() - 1; layer_idx++)
	 {
		 cur_out.resize(configuration[layer_idx + 1], 0);

		 for (size_t to_idx = 0; to_idx < configuration[layer_idx + 1]; to_idx++)
		 {
			 for (size_t from_idx = 0; from_idx < configuration[layer_idx]; from_idx++)
			 {
				 cur_out[to_idx] += weights[layer_idx][from_idx][to_idx] * prev_out[from_idx];
			 }
			 cur_out[to_idx] = Activation(cur_out[to_idx]);
		 }
		 prev_out = cur_out;
	 }

	 return prev_out;
}

float ANN::BackPropTraining(std::shared_ptr<ANN::ANeuralNetwork> ann, std::vector<std::vector<float>>& inputs, std::vector<std::vector<float>>& outputs, int maxIters, float eps, float speed, bool std_dump)
 {
	
	ann->RandomInit();
	float Error;	
	int Iters_count = 0;
	do {
		Error = 0.0;
		for (int j = 0; j < inputs.size(); j++)
			Error += BackPropTrainingIteration(ann, inputs[j], outputs[j], speed);
		Error /= (float)(inputs.size());
		Iters_count++;
		if (std_dump && (Iters_count % 100 == 0))cout <<"Iteration: "<< Iters_count << "\t Error:  "<< Error << endl;
		if (Error < eps) break;
	} while (Iters_count < maxIters);
	cout << "Iteration: " << Iters_count << "\t Error:  " << Error << endl;
	ann->is_trained = true;
	return Error;
	
 }

float ANN::BackPropTrainingIteration(std::shared_ptr<ANN::ANeuralNetwork> ann, const std::vector<float>& input,
	const std::vector<float>& output,float speed)
{
	vector<vector<float>> lineconf = vector<vector<float>>(ann->configuration.size());
	lineconf[0] = input;
	for (int i = 1; i < lineconf.size(); i++)
	{
		lineconf[i] = vector<float>(ann->configuration[i]);
		for (int j = 0; j < lineconf[i].size(); j++)
		{
			lineconf[i][j] = 0.0f;
			for (int k = 0; k < lineconf[i - 1].size(); k++)
				lineconf[i][j] += lineconf[i - 1][k] * ann->weights[i - 1][k][j];
			lineconf[i][j] = ann->Activation(lineconf[i][j]);
		}
	}
	//��������� �����
	vector<float> old_vesa = vector<float>(ann->configuration.back());
	int iter_count = ann->weights.size() - 1;
	for (int i = 0; i < ann->configuration[iter_count + 1]; i++)
	{
		old_vesa[i] = (lineconf[iter_count + 1][i] - output[i]);
		old_vesa[i] *= ann->ActivationDerivative(lineconf[iter_count + 1][i]);
		for (int j = 0; j < ann->configuration[iter_count]; j++)
			ann->weights[iter_count][j][i] -= speed * old_vesa[i] * lineconf[iter_count][j];
	}
	// ��������� ����� � �����
	// �� ����� ��������:
	while (iter_count > 0)
	{
		iter_count--;
		vector<float> new_vesa = vector<float>(ann->configuration[iter_count + 1]);
		for (int i = 0; i < ann->configuration[iter_count + 1]; i++)
		{
			new_vesa[i] = 0.0;
			for (int k = 0; k < ann->configuration[iter_count + 2]; k++)
				new_vesa[i] += old_vesa[k] * ann->weights[iter_count + 1][i][k];
			new_vesa[i] *= ann->ActivationDerivative(lineconf[iter_count + 1][i]);
			for (int j = 0; j < ann->configuration[iter_count]; j++)
				ann->weights[iter_count][j][i] -= speed * new_vesa[i] * lineconf[iter_count][j];
		}
		old_vesa = new_vesa;
	}
	
	lineconf[0] = input;
	for (int i = 1; i < lineconf.size(); i++)
	{
		for (int j = 0; j < lineconf[i].size(); j++)
		{
			lineconf[i][j] = 0.0f;
			for (int k = 0; k < lineconf[i - 1].size(); k++)
				lineconf[i][j] += lineconf[i - 1][k] * ann->weights[i - 1][k][j];
			lineconf[i][j] = ann->Activation(lineconf[i][j]);
		}
	}
	//���������
	float Error = 0.0f;
	for (int i = 0; i < lineconf.back().size(); i++)
		Error += (lineconf.back()[i] - output[i]) * (lineconf.back()[i] - output[i]);
	return Error / 2;

}