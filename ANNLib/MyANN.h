#pragma once
/**/
#ifdef ANNDLL_EXPORTS
#define ANNDLL_API __declspec(dllexport) 
#else
#define ANNDLL_API __declspec(dllimport) 
#endif*/

#include <vector>
#include <memory>
#include <string>
#include <ANN.h>

using namespace ANN;
using namespace std;

class MyANN : public ANeuralNetwork
{
public:
	MyANN(
		vector<size_t> configuration,
		float scale,
		ActivationType activation_type
	)
	{
		this->configuration = configuration;
		this->scale = scale;
		this->activation_type = activation_type;
	}

	//2)���������� GetType
	//3)���������� Predict
	// ������������ ����� ANeuralNetwork
	virtual ANNDLL_API std::string GetType() override;
	virtual ANNDLL_API std::vector<float> Predict(std::vector<float>& input) override;
	
};

